# core-parser
## A parser for the language Core written in Haskell
### Filippo Berto - University of Pauda (Università degli Studi di Padova) - AA 2017/2018
[![pipeline status](https://gitlab.com/bertof/Core-lang-parser/badges/master/pipeline.svg)](https://gitlab.com/bertof/Core-lang-parser/commits/master)

This project is part of the assignment of the course of Functional Language of the Master Degree in Computer Science. The goal is to write a parser for the functional programming language Core in Haskell.