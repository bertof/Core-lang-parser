module Core.ParseProg where

import Core.Types
import Core.Expressions
import Core.Parser
import Control.Applicative

parseProg :: Parser (Program Name)
parseProg = do
  p <- parseScDef
  do
    symbol ";"
    ps <- parseProg
    return (p:ps)
    <|> return [p]

parseScDef :: Parser (ScDefn Name)
parseScDef = do
  v <- var
  pf <- many $ var
  symbol "="
  body <- parseExpr
  return (v, pf, body)