{-# LANGUAGE LambdaCase #-}

module Core.Parser where

import Control.Applicative as CA
import Data.Char

-- *********************
-- * PARSER DEFINITION *
-- *********************

newtype Parser a = P(String -> [(a, String)])

parse :: Parser a -> String -> [(a, String)]
parse (P p) = p

instance Functor Parser where
  fmap f px = do
    x <- px
    return (f x)

instance Applicative Parser where
  pure v = P(\input -> [(v, input)])
  pf <*> px = do
    f <- pf
    x <- px
    return (f x)

instance Monad Parser where
  p >>= f = P $
    \input ->
    case parse p input of
      [] -> []
      [(value, rest)] -> parse (f value) rest

instance CA.Alternative Parser where
  empty = P(\input -> [])

  p <|> q = P $
    \input ->
    case parse p input of
      [] -> parse q input
      [(value , rest)] -> [(value, rest)]


-- ***********************
-- * SPECIALIZED PARSERS *
-- ***********************

-- Char parser
item :: Parser Char
item = P $
  \case
    [] -> []
    (x:xs) -> [(x,xs)]

-- Parse a char and return it only if the function applied to it returns True
sat :: (Char -> Bool) -> Parser Char
sat p = do
  x <- item
  if p x then
    return x
  else
    empty

digit :: Parser Char
digit = sat isDigit

lower :: Parser Char
lower = sat isLower

alpha :: Parser Char
alpha = sat isAlpha

alphaNum :: Parser Char
alphaNum = sat isAlphaNum

-- Char equal to x::Char
char :: Char -> Parser Char
char x = sat (==x)

-- String equal to x::String
string :: String -> Parser String
string [] = return []
string (x:xs) = do
  char x
  string xs
  return (x:xs)

-- Variable name Parser (lower:alphaNum)
ident :: Parser String
ident = do
  x <- lower
  xs <- many alphaNum
  return (x:xs)

spaces :: Parser ()
spaces = do
  many (sat isSpace)
  return ()

nat :: Parser Int
nat = do
  xs <- some digit
  return (read xs)

int :: Parser Int
int = do
  char '-'
  n <- nat
  return (-n)
  <|> nat

token :: Parser a -> Parser a
token p = do
  spaces
  v <- p
  spaces
  return v

-- *****************
-- * TOKEN PARSERS *
-- *****************

identity :: Parser String
identity = token ident

natural :: Parser Int
natural = token nat

integer :: Parser Int
integer = token int

symbol :: String -> Parser String
symbol xs = token (string xs)
