module Core.Expressions where

import Control.Applicative

import Core.Types
import Core.Parser

varCharacter :: Parser Char
varCharacter = alphaNum <|> char '_'

var :: Parser String
var = token $ do
  x <- alpha
  xs <- many varCharacter
  return (x:xs)

underscore :: Parser String
underscore = symbol "_"

-- ********************
-- * CORE EXPRESSIONS *
-- ********************

parseExpr :: Parser (Expr Name)
parseExpr =
  parseLet
  <|> parseLetRec
  <|> parseCase
  <|> parseLambda
  <|> parseBinApp

-- Parser for non recursive let expressions
parseLet :: Parser (Expr Name)
parseLet = do
  symbol "let"
  ds <- parseMultipleDefinitions
  symbol "in"
  e <- parseExpr
  return (ELet NonRecursive ds e)

-- Parser for recursive let expressions
parseLetRec :: Parser (Expr Name)
parseLetRec = do
  symbol "letrec"
  ds <- parseMultipleDefinitions
  symbol "in"
  e <- parseExpr
  return (ELet Recursive ds e)

-- Parser for multiple (concatenated) definitions
parseMultipleDefinitions :: Parser [Def Name]
parseMultipleDefinitions = do
  d <- parseDefinition
  ds <- many $ do
    symbol ";" <|> symbol "\n"
    parseDefinition

  return (d:ds)

-- Parser for variables definitions
parseDefinition :: Parser (Def Name)
parseDefinition = do
  variable <- var
  symbol "="
  expression <- parseExpr
  return (variable, expression)

-- Parser for case expressions
parseCase :: Parser (Expr Name)
parseCase = do
  symbol "case"
  e <- parseExpr
  symbol "of"
  as <- parseAlts
  return (ECase e as)

-- Parser for an alternative
parseAlt :: Parser (Alter Name)
parseAlt = do
  symbol "<"
  index <- natural
  symbol ">"
  do
    vars <- many var
    symbol "->"
    exp <- parseExpr
    return (index, vars, exp)
    <|> do
      s <- underscore
      symbol "->"
      exp <- parseExpr
      return (index, [s], exp)

-- Parser for alternatives
parseAlts :: Parser [Alter Name]
parseAlts = do
  a <- parseAlt
  as <- many $ do
    symbol ";" <|> symbol "\n"
    parseAlt
  return (a:as)

-- Parser for lambdas
parseLambda :: Parser (Expr Name)
parseLambda = do
  symbol "\005C" <|> symbol "\\"  -- Backslash symbol
  vs <- some var
  symbol "\002E" <|> symbol "." -- Dot symbol
  e <- parseExpr
  return (ELam vs e)

-- Parser for atomic expressions
parseAExpr :: Parser (Expr Name)
parseAExpr =
  parseVar
  <|> parseNum
  <|> parseConst
  <|> parseParExpr

-- Parser for a variable
parseVar :: Parser (Expr Name)
parseVar = do
  v <- var
  if isKeyword v then
    empty
  else return (EVar v)

isKeyword :: String -> Bool
isKeyword v = v `elem` ["in", "case", "let", "letrec", "of"]

parseNum :: Parser (Expr Name)
parseNum = do
  n <- integer
  return (ENum n)

parseConst :: Parser (Expr Name)
parseConst = do
  symbol "Pack"
  symbol "{"
  tag <- natural
  symbol ","
  arity <- natural
  symbol "}"
  return (EConstr tag arity)

-- Parser for parenthesized expression
parseParExpr :: Parser (Expr Name)
parseParExpr = do
  symbol "("
  e <- parseExpr
  symbol ")"
  return e

parseBinApp :: Parser (Expr Name)
parseBinApp= do
  t1 <- parseExpr2
  do
    s <- symbol "|"
    t2 <- parseBinApp
    return (EAp(EAp(EVar s) t1) t2)
    <|> return t1

parseExpr2 :: Parser (Expr Name)
parseExpr2 = do
  t1 <- parseExpr3
  do
    s <- symbol "&"
    t2 <- parseExpr2
    return (EAp(EAp(EVar s) t1) t2)
    <|> return t1

parseExpr3 :: Parser (Expr Name)
parseExpr3 = do
  t1 <- parseExpr4
  do
    o <- relationalOpSymbol
    t2 <- parseExpr3
    return (EAp(EAp(EVar o) t1) t2)
    <|> return t1

relationalOpSymbol :: Parser String
relationalOpSymbol =
  symbol "<="
  <|> symbol "<"
  <|> symbol "=="
  <|> symbol "~="
  <|> symbol ">="
  <|> symbol ">"

parseExpr4 :: Parser (Expr Name)
parseExpr4 = do
  t1 <- parseExpr5
  do
    s <- symbol "+"
    t2 <- parseExpr4
    return (EAp(EAp(EVar s) t1) t2)
    <|> do
          s <- symbol "-"
          t2 <- parseExpr4
          return (EAp(EAp(EVar s) t1) t2)
    <|> return t1

parseExpr5 :: Parser (Expr Name)
parseExpr5 = do
  t1 <- parseExpr6
  do
    s <- symbol "*"
    t2 <- parseExpr5
    return (EAp(EAp(EVar s) t1) t2)
    <|> do
      s <- symbol "/"
      t2 <- parseExpr5
      return (EAp(EAp(EVar s) t1) t2)
    <|> return t1

parseExpr6 :: Parser (Expr Name)
parseExpr6 = do
  a <- parseAExpr
  as <- many parseAExpr
  return (foldl(\a b -> (EAp a b)) a as)
