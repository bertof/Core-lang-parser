module Core.Types where

type Name = String

data Expr a
  = EVar Name                         --  Variables
  | ENum Int                          --  Numbers
  | EConstr Int Int                   --  Constructor tag arity
  | EAp (Expr a) (Expr a)             --  Application
  | ELet                              --  Let(rec) expression
      IsRec                           --    Boolean with True = recursive
      [Def a]                         --    Definitions
      (Expr a)                        --    Body of let(rec)
  | ECase                             --  Case expression
      (Expr a)                        --    Expression to scrutinize
      [Alter a]                       --    Alternatives
  | ELam [a] (Expr a)                 --  Lambda abstractions
  deriving (Show, Eq)

type Program a = [ScDefn a]           -- A Program is composed by a list of super-combinators
type CoreProgram = Program Name       -- A CoreProgram is a Program with a name
type ScDefn a = (Name, [a], Expr a)   -- A SuperCombinatorDefinition contains its name, arguments and body
type CoreScDefn = ScDefn Name         -- A CoreSuperCombinatorDefinition with a name
type Def a = (a, Expr a)              -- for let and let rec
type Alter a = (Int, [a], Expr a)     -- for case

data IsRec = NonRecursive | Recursive -- Boolean to define if expression is recursive
  deriving (Show, Eq)
