module Core.ExpressionsSpec where

import Test.Hspec
import Test.QuickCheck

import Core.Parser
import Core.Expressions
import Core.Types

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "var" $
    it "parses a variable name (identifier)" $ do
      parse var "abc" `shouldBe` [("abc", "")]
      parse var "a_" `shouldBe` [("a_", "")]
      parse var "a1" `shouldBe` [("a1", "")]
      parse var "aB" `shouldBe` [("aB", "")]
      parse var " a " `shouldBe` [("a", "")]
      parse var "1a" `shouldBe` []
      parse var "_a" `shouldBe` []
      parse var "Ab" `shouldBe` [("Ab", "")]

  describe "parseLet" $
    it "parses a let expression" $ do
      (parse parseLet "let x = 5 in x") `shouldBe` [(ELet NonRecursive [("x",ENum 5)] (EVar "x"),"")]
      (parse parseLet "let x = 5; y = (3 * x) in x") `shouldBe` [(ELet NonRecursive [("x",ENum 5),("y",EAp (EAp (EVar "*") (ENum 3)) (EVar "x"))] (EVar "x"),"")]

  describe "parseLetRec" $
    it "parses a letRec expression" $ do
      (parse parseLetRec "letrec x = 5 in x") `shouldBe` [(ELet Recursive [("x",ENum 5)] (EVar "x"),"")]
      (parse parseLetRec "letrec x = 5; y = x in x") `shouldBe` [(ELet Recursive [("x",ENum 5),("y",EVar "x")] (EVar "x"),"")]

  describe "parseDefinition" $
    it "parses a definition expression" $ do
      (parse parseDefinition "x = 3") `shouldBe` [(("x",ENum 3),"")]
      (parse parseDefinition "x = y") `shouldBe` [(("x",EVar "y"),"")]
      (parse parseDefinition "x = (y + 1)") `shouldBe` [(("x",EAp (EAp (EVar "+") (EVar "y")) (ENum 1)),"")]
      (parse parseDefinition "x = (1 + y)") `shouldBe` [(("x",EAp (EAp (EVar "+") (ENum 1)) (EVar "y")),"")]

      (parse parseDefinition "x = (3)") `shouldBe` [(("x",ENum 3),"")]
      (parse parseDefinition "x = (3 * y)") `shouldBe` [(("x",EAp (EAp (EVar "*") (ENum 3)) (EVar "y")),"")]
      (parse parseDefinition "x = (3 / y)") `shouldBe` [(("x",EAp (EAp (EVar "/") (ENum 3)) (EVar "y")),"")]
      (parse parseDefinition "x = (3 + y)") `shouldBe` [(("x",EAp (EAp (EVar "+") (ENum 3)) (EVar "y")),"")]
      (parse parseDefinition "x = (3 - y)") `shouldBe` [(("x",EAp (EAp (EVar "-") (ENum 3)) (EVar "y")),"")]

  describe "parseMultipleDefinitions" $
    it "parses some definitions" $ do
      (parse parseMultipleDefinitions "x = 3") `shouldBe` [([("x",ENum 3)],"")]
      (parse parseMultipleDefinitions "x = 3; y = 5") `shouldBe` [([("x",ENum 3),("y",ENum 5)],"")]
      (parse parseMultipleDefinitions "x = 3; y = (3 * x)") `shouldBe` [([("x",ENum 3),("y",EAp (EAp (EVar "*") (ENum 3)) (EVar "x"))],"")]
      (parse parseMultipleDefinitions "x = 3; y = (x * 3); z = (3 * y)") `shouldBe` [([("x",ENum 3),("y",EAp (EAp (EVar "*") (EVar "x")) (ENum 3)),("z",EAp (EAp (EVar "*") (ENum 3)) (EVar "y"))],"")]
      (parse parseMultipleDefinitions "x = 3; y = (x * 3) in (x + z)") `shouldBe` [([("x",ENum 3),("y",EAp (EAp (EVar "*") (EVar "x")) (ENum 3))],"in (x + z)")]

  describe "parseCase" $
    it "parses a case expression" $ do
      (parse parseCase "case x of <1> _ -> 5; <2> x -> 10") `shouldBe` [(ECase (EVar "x") [(1,["_"],ENum 5),(2,["x"],ENum 10)],"")]
      (parse parseCase "case t of <1> _ -> 0; <2> t1 t2 -> 1 + max (depth t1) (depth t2)") `shouldBe` [(ECase (EVar "t") [(1,["_"],ENum 0),(2,["t1","t2"],EAp (EAp (EVar "+") (ENum 1)) (EAp (EAp (EVar "max") (EAp (EVar "depth") (EVar "t1"))) (EAp (EVar "depth") (EVar "t2"))))],"")]

  describe "parseAlt" $
    it "parses a alternative expression of case expressions" $ do
      (parse parseAlt "<1> x -> 0") `shouldBe` [((1,["x"],ENum 0),"")]
      (parse parseAlt "<1> x y -> 0") `shouldBe` [((1,["x","y"],ENum 0),"")]
      (parse parseAlt "<1> _ -> 0") `shouldBe` [((1,["_"],ENum 0),"")]
      (parse parseAlt "<1> _ y -> 4") `shouldBe` []

  describe "parseAlts" $
    it "parses the alternative expressions of case expressions" $ do
      (parse parseAlts "<1> x -> 0; <2> x -> 2") `shouldBe` [([(1,["x"],ENum 0),(2,["x"],ENum 2)],"")]
      (parse parseAlts "<1> x y -> 0; <4> x z -> 1") `shouldBe` [([(1,["x","y"],ENum 0),(4,["x","z"],ENum 1)],"")]
      (parse parseAlts "<1> _ y -> 4; <3> _ x -> 6") `shouldBe` []
      (parse parseAlts "<1> _ y -> 4 <3> _ x -> 6") `shouldBe` []

  describe "parseLambda" $
    it "parses the a lambda expression" $ do
      (parse parseLambda "\\ x . x + 1") `shouldBe` [(ELam ["x"] (EAp (EAp (EVar "+") (EVar "x")) (ENum 1)),"")]
      (parse parseLambda "\\ x y z . x + y + z") `shouldBe` [(ELam ["x","y","z"] (EAp (EAp (EVar "+") (EVar "x")) (EAp (EAp (EVar "+") (EVar "y")) (EVar "z"))),"")]

  describe "parseConst" $
    it "parses a constructor expression" $ do
      parse parseConst "Pack { 1 , 2 }" `shouldBe` [((EConstr 1 2), "")]
      parse parseConst "Pack { 1 ,  }" `shouldBe` []
      parse parseConst "Pack { 1 2 }" `shouldBe` []

  describe "parseExpr" $
    it "parses an expression" $ do
      (parse parseExpr "1 + 2 * 3") `shouldBe` [(EAp (EAp (EVar "+") (ENum 1)) (EAp (EAp (EVar "*") (ENum 2)) (ENum 3)),"")]
      (parse parseExpr "1 * 2 + 3") `shouldBe` [(EAp (EAp (EVar "+") (EAp (EAp (EVar "*") (ENum 1)) (ENum 2))) (ENum 3),"")]
      (parse parseExpr "1 / 2 + 3") `shouldBe` [(EAp (EAp (EVar "+") (EAp (EAp (EVar "/") (ENum 1)) (ENum 2))) (ENum 3),"")]
      (parse parseExpr "1 * 2 / 3") `shouldBe` [(EAp (EAp (EVar "*") (ENum 1)) (EAp (EAp (EVar "/") (ENum 2)) (ENum 3)),"")]
      (parse parseExpr "1 < 2") `shouldBe` [(EAp (EAp (EVar "<") (ENum 1)) (ENum 2),"")]
      (parse parseExpr "1 < 2 & 2 >= 1") `shouldBe` [(EAp (EAp (EVar "&") (EAp (EAp (EVar "<") (ENum 1)) (ENum 2))) (EAp (EAp (EVar ">=") (ENum 2)) (ENum 1)),"")]
      (parse parseExpr "1 < 2 | 2 >= 1") `shouldBe` [(EAp (EAp (EVar "|") (EAp (EAp (EVar "<") (ENum 1)) (ENum 2))) (EAp (EAp (EVar ">=") (ENum 2)) (ENum 1)),"")]
      (parse parseExpr "x y") `shouldBe` [(EAp (EVar "x") (EVar "y"),"")]