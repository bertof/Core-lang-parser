module Core.ParserSpec where

import Test.Hspec
import Test.QuickCheck

import Data.Char

import Core.Parser

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "item" $
    it "parses a char" $ do
      parse (char 'a') "abcd" `shouldBe` [('a', "bcd")]
      parse (char 'a') [] `shouldBe` []

  describe "sat" $
    it "parses a char that is validated by the function passed" $ do
    parse (sat (=='a')) "abcd" `shouldBe` [('a', "bcd")]
    parse (sat (const True)) "abcd" `shouldBe` [('a', "bcd")]
    parse (sat (const False)) "abcd" `shouldBe` []
    parse (sat (const True)) [] `shouldBe` []
    parse (sat isSpace) " abc" `shouldBe` [(' ', "abc")]

  describe "space" $
    it "parses many spaces" $ do
      parse spaces "abc" `shouldBe` [((),"abc")]
      parse spaces " abc" `shouldBe` [((),"abc")]
      parse spaces "   abc" `shouldBe` [((),"abc")]

  describe "string" $
    it "parses a string" $ do
      parse (string "ab") "abc" `shouldBe` [("ab","c")]
      parse (string "ab") "  abc" `shouldBe` []
      parse (string "ab") "" `shouldBe` []
      parse (string "") "" `shouldBe` [("", "")]

  describe "ident" $
    it "parses a name of variable" $ do
       parse ident "abc" `shouldBe` [("abc","")]
       parse ident "aBC" `shouldBe` [("aBC","")]
       parse ident "3ab" `shouldBe` []
       parse ident "Abc" `shouldBe` []
       parse ident " abc" `shouldBe` []

  describe "nat" $
    it "parses a nat number" $ do
       parse nat "1" `shouldBe` [(1,"")]
       parse nat "100" `shouldBe` [(100,"")]
       parse nat "aBC" `shouldBe` []
       parse nat "3ab" `shouldBe` [(3,"ab")]
       parse nat "-5" `shouldBe` []

  describe "int" $
    it "parses an int number" $ do
       parse int "1" `shouldBe` [(1,"")]
       parse int "100" `shouldBe` [(100,"")]
       parse int "aBC" `shouldBe` []  -- Not an integer
       parse int "3ab" `shouldBe` [(3,"ab")]
       parse int "-5ac" `shouldBe` [(-5, "ac")]
       parse int "-50ac" `shouldBe` [(-50, "ac")]

  describe "token" $
    it "tokenizes a parser" $ do
      parse (token int) "-5" `shouldBe` [(-5, "")]
      parse (token int) " -5" `shouldBe` [(-5, "")]
      parse (token int) "-5 " `shouldBe` [(-5, "")]
      parse (token int) " -5 " `shouldBe` [(-5, "")]
      parse (token int) " -5 ac" `shouldBe` [(-5, "ac")]
      parse (token int) " -5ac" `shouldBe` [(-5,"ac")]
      parse (token int) "-5 ac" `shouldBe` [(-5,"ac")]
      parse (token int) "-5ac" `shouldBe` [(-5,"ac")]
      parse (token $ sat $ const True) "  a  " `shouldBe` [('a', "")]

  describe "identity" $
    it "parses a tokenized name of variable" $ do
      parse identity "abc" `shouldBe` [("abc","")]
      parse identity "aBC" `shouldBe` [("aBC","")]
      parse identity "3ab" `shouldBe` [] -- Must start with lower letter
      parse identity "Abc" `shouldBe` [] -- Must start with lower letter
      parse identity "abc" `shouldBe` [("abc","")]

  describe "natural" $
    it "parses a natural number" $ do
      parse natural "1" `shouldBe` [(1,"")]
      parse natural "100" `shouldBe` [(100,"")]
      parse natural "aBC" `shouldBe` []
      parse natural "3ab" `shouldBe` [(3,"ab")]
      parse natural "-5" `shouldBe` []

  describe "integer" $
    it "parses an integer number" $ do
      parse integer "1" `shouldBe` [(1,"")]
      parse integer "100" `shouldBe` [(100,"")]
      parse integer "aBC" `shouldBe` []  -- Not an integer
      parse integer "3ab" `shouldBe` [(3,"ab")]
      parse integer "-5ac" `shouldBe` [(-5, "ac")]
      parse integer "-50ac" `shouldBe` [(-50, "ac")]

  describe "symbol" $
    it "parses a symbol" $ do
      parse (symbol "ab") "abc" `shouldBe` [("ab","c")]
      parse (symbol "ab") "  abc" `shouldBe` [("ab","c")]
      parse (symbol "ab") "  abc  " `shouldBe` [("ab","c  ")]
      parse (symbol "ab") "" `shouldBe` []