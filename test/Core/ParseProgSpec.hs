module Core.ParseProgSpec where

import Test.Hspec
import Test.QuickCheck
import System.IO

import Core.Parser
import Core.ParseProg
import Core.Expressions
import Core.Types

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "parseProg" $ do
    it "parses a string as a program without leaving any remaining input" $ do
      let [(output, res)] = parse parseProg "f = 3; g x y = let z = x in z; h x = case (let y = x in y) of <1> -> 2; <2> -> 5"
      show output `shouldBe` "[(\"f\",[],ENum 3),(\"g\",[\"x\",\"y\"],ELet NonRecursive [(\"z\",EVar \"x\")] (EVar \"z\")),(\"h\",[\"x\"],ECase (ELet NonRecursive [(\"y\",EVar \"x\")] (EVar \"y\")) [(1,[],ENum 2),(2,[],ENum 5)])]"
      res `shouldBe` ""

    it "parses the file input.txt correctly" $ do
      fileContent <- readFile "input.txt"
      let [(output, res)] = parse parseProg fileContent
      show output `shouldBe` "[(\"f\",[],ENum 3),(\"g\",[\"x\",\"y\"],ELet NonRecursive [(\"z\",EVar \"x\")] (EVar \"z\")),(\"h\",[\"x\"],ECase (ELet NonRecursive [(\"y\",EVar \"x\")] (EVar \"y\")) [(1,[],ENum 2),(2,[],ENum 5)])]"
      res `shouldBe` ""