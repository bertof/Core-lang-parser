module Main where

import System.IO

import Core.Parser
import Core.ParseProg
import Core.Expressions
import Core.Types

main :: IO (Program Name)
main = do
  fileContent <- readFile "input.txt"
  return (comp $ parse parseProg fileContent)

comp :: [(Program Name, String)] -> Program Name
comp [] = error "Parser didn't work!"
comp [(e, [])] = e
comp [(_, a)] = error $ "Not all input used: " ++ a
